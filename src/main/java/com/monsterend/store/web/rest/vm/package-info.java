/**
 * View Models used by Spring MVC REST controllers.
 */
package com.monsterend.store.web.rest.vm;
