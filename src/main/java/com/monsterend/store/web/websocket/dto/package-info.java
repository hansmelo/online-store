/**
 * Data Access Objects used by WebSocket services.
 */
package com.monsterend.store.web.websocket.dto;
