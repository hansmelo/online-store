package com.monsterend.store.domain.enumeration;

/**
 * The OrderItemStatus enumeration.
 */
public enum OrderItemStatus {
    AVAILABLE, OUT_OF_STOCK, BACK_ORDER
}
