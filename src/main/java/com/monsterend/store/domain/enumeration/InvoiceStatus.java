package com.monsterend.store.domain.enumeration;

/**
 * The InvoiceStatus enumeration.
 */
public enum InvoiceStatus {
    PAID, ISSUED, CANCELLED
}
