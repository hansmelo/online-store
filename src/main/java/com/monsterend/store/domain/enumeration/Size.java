package com.monsterend.store.domain.enumeration;

/**
 * The Size enumeration.
 */
public enum Size {
    S, M, L, XL, XXL
}
